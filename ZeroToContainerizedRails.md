## 0 to Containerized Rails in 5 minutes

You can create a new rails application, and bootstrap it for local container
development, with or without VSCode Remote Container Development, in 5 minutes.
Here is how:

optional: Create gitlab project and clone empty repo (see Glab Example below)

1. Use a ruby base image to launch a docker container on your host, with a volume mount to the parent directory where you want to create your new rails project (e.g. if you want your project to be in $HOME/projects/new-rails-project volume mount $HOME/projects)
```
cd $HOME/projects
docker run -u $(id -u):$(id -g) -ti --rm -v${HOME}/projects:/projects --workdir /projects ruby:2.7.2 bash
```

2. Inside this container, install rails (you can install a specific version
of rails using gem install rails -v ${VERSION}):
```
gem install rails
```

3. Inside the container, run rails new. It is recommended that you
--skip-bundle, since this is a disposable container. You can also
choose the database, enable/disable rails features, use api only,
etc. (run rails new -h for details). Exit the container after you
create the new rails environment:
```
rails new --skip-bundle -d postgresql zero_to_rails
exit
```

4. cd into the new project directory
```
cd zero_to_rails
```

5. Detect project attributes
```
curl https://gitlab.oit.duke.edu/rja3/developer-tools/-/raw/v1.5.2/detect-project-attributes.sh | bash
```

6. bootstrap local container
development:
```
curl https://gitlab.oit.duke.edu/rja3/developer-tools/-/raw/v1.5.2/bootstrap-container-development.sh | bash
```

7. Choose whether you want to use VSCode Remote Container Development, 
or docker-compose, for local development

#### docker-compose
1. build the development image using the Dockerfile. This will bundle
install the gems into the image
```
docker-compose build server
```

2. Ensure that the variables in `server.env` fit with what your 
`config/database.yml` expects. You may have to add the following lines to the default entry (you can do this while the server is building):
```
username: <%= ENV['DATABASE_USER'] %>
password: <%= ENV['DATABASE_PASSWORD'] %>
host: <%= ENV['DATABASE_HOST'] %>
```

3. start the db
```
docker-compose up -d postgres
```

4. create and migrate the database
```
docker-compose run --rm rails db:create
docker-compose run --rm rails db:migrate
```

Alternatively, you can use the aliases in bin/docker-compose within your terminal session to make calls to rails, rake, etc. use `docker-compose run --rm $command`
under the hood. This will not affect your other terminal sessions, e.g. rails, rake, etc. will use any locally installed command in other terminals.
```
source bin/docker-compose/aliases
rails db:create
rails db:migrate
````

5. bring up the server
```
docker-compose up -d server
```

Alternatively, you can use bin/docker-compose/start-server.sh to run steps 3, 4, and 5.

#### View the Application

Open http://localhost:3000 in your browser. (If you have any other
rails projects that use 3000, you can edit the root docker.compose.yml to change the port to a different port)

#### Clean up

Stop and remove the running containers for this project.
```
docker-compose down
```

#### VSCode Remote Container Development

1. bootstrap its configuration:
```
curl https://gitlab.oit.duke.edu/rja3/developer-tools/-/raw/v1.5.2/bootstrap-vscode.sh | bash
```

2. Launch VSCode. If you are on a mac, you might want to 
[install the `code` executable](https://code.visualstudio.com/docs/setup/mac#_launching-from-the-command-line) after you open VSCode using its standard graphical launcher. You can then launch a new VSCode window
for each project from the commandline from within the root of the project
```
code .
```

You must [install the Remote Container Development Extension](https://code.visualstudio.com/docs/remote/containers-tutorial#_install-the-extension) if you have not already.
Once you have, you should get a prompt to `Reopen in Container`, click
that button, and it will launch you into a terminal in your VSCode window
inside the container.

3. bundle install your Gems. Now you can do all the rails commands that
you would expect in any of the terminals that are open in your VSCode
window. 

4. Ensure that the variables in `server.env` fit with what your 
`config/database.yml` expects. You may have to add the following lines to the default entry (you can do this in VSCode, while bundle install is running):
```
username: <%= ENV['DATABASE_USER'] %>
password: <%= ENV['DATABASE_PASSWORD'] %>
host: <%= ENV['DATABASE_HOST'] %>
```

5. create and migrate the database, then launch puma
```
rails db:create
rails db:migrate
puma
```

6. Consult the VSCode.md file that was installed by bootstrap-vscode.sh into your project root for more information about developing in a 
container with VSCode.

Do not run docker-compose down unless you need to start from scratch with your development environment (e.g you will have to bundle install
again)

8. Commit
If this is a real project, you should commit all of the developer-tools changes to your repo.

## Glab Example

You can install [glab](https://github.com/profclems/glab) on your machine
and use it to interact with your gitlab instances, including listing
issues, merge requests, etc.

Here is how you would create and clone a new, empty project for the new rails
application created by 0 to rails in 5 minutes.

1. Create a [personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) and save it to a secure file
2. set the token to the GITLAB_API_TOKEN environment variable and set GITLAB_HOST (do not include protocol!)
```
export GITLAB_HOST=gitlab.oit.duke.edu
export GITLAB_TOKEN=$(cat $pathtofile)
```
3. create project in desired group. Since the default visibility is internal,
you should set it explicitly to -p for private or -P for public. If you do
not set a group, it will create in your personal namespace. Choose 'n' when
prompted to git init. If you plan on creating a personal fork, also choose 'n'
when prompted to clone a local copy. You can override the default branch (default
master) as well.
```
glab project create [--defaultBranch main] [-p|-P] [-g groupname, can include subgroups] project_name
```

4b. Fork to your personal namespace if you want to do collaborative development
using personal kubernetes deployments and/or merge request code reviews.
`upstream_name` should include namespace and name, e.g. group1/group2/projectname
if in a subgroup of a group. Then clone your personal fork.
```
glab project fork upstream_name
glab project clone project_name
```
This will create 2 remotes in the cloned repo, origin for your personal
fork, and upstream for the upstream of the fork. Now you can run the 0
to rails example into an existing git repo.

## Scanning Gitlab Project Container Images with Twistloc

1. create a personal access token with read-registry access only, save that in your home directory
`~/.image-scan-token`
2. Alternatively, you can create a project access token with read-registry for each project, save it to a file,
and set the environment variable IMAGE_SCAN_TOKEN_FILE.
3. For either, you shuold chmod 600 to lock access to this file to only your user
4. Run the following
```
curl https://gitlab.oit.duke.edu/rja3/developer-tools/-/raw/v1.5.2/scan-image | bash
```

If you have jq installed, you can pipe the output to a file, and use jq to parse it
```
curl https://gitlab.oit.duke.edu/rja3/developer-tools/-/raw/v1.5.2/scan-image | bash > image-scan.out
jq '.ScanResults.results[0].vulnerabilityScanPassed' image-scan.out
```
