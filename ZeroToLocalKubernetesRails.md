## 0 to Rails in Local Kubernetes in 5 minutes

You can start using Kubernetes for local development using this example.
Before starting, work through steps 1-6 of the [0 to Containerized Rails in 5 minutes](https://gitlab.oit.duke.edu/rja3/developer-tools/-/blob/main/ZeroToContainerizedRails.md) example.
Then you can add kubernetes to your containerized rails application.

### Requirements

To complete this example, you will need to [Install Ansible](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html) if you have not already installed it.

### Steps

1. Install Kubernetes

If you are on a Mac, refer to the [Mac README](local_kubernetes/Darwin/Readme.md)

If you are on a Linux, use k3s:
```
curl https://gitlab.oit.duke.edu/rja3/developer-tools/-/raw/v1.5.2/install-local-kubernetes.sh | bash
```
Ensure kubernetes is running:
kubectl get nodes

The node should be Ready

You may want to also install [Lens Desktop](https://k8slens.dev/desktop.html) for a view
into the local kubernetes cluster, and any other kubernetes cluster we work with (such as
DHE Openshift), that is very similar to the Openshift web GUI. You can see all kubernetes
resources, Pod logs, and even get a terminal into a running Pod.

2. Bootstrap your application for local kubernetes development
```
curl https://gitlab.oit.duke.edu/rja3/developer-tools/-/raw/v1.5.2/bootstrap-local-kubernetes-development.sh | bash
```

3. source aliases
```
source bin/local-kube/aliases
```

4. build your container image (this will ask for sudo password on linux)
```
bin/local-kube/build.sh
```

5. deploy your image to local kubernetes. Visit the url printed out at the end of the deployment. You will need
to type in your password for sudo.
```
bin/local-kube/deploy.sh
```

6. See what is deployed with kubectl and helm
```
kubectl config current-context
kubectl get deployments
kubectl get pods
kubectl get services
kubectl get ingresses
kubectl get all
helm list
```
Note, the ingress has the url that you visited in your browser as the host.

7. migrate and seed your database
```
rails db:migrate
seed
```

8. Make changes to your code, reload the browser to see changes

9. Add a gem to your gemfile, bundle (this actually bundles both your puma pod, and your rspec pod), and restart
your puma server to get the new gems
```
bundle_all
restart_server
```

10. get a terminal in your puma container
```
server_console
exit
```

11. get a rails console
```
rails c
exit
```

12. decommission
```
bin/local-kube/decommission.sh
```

