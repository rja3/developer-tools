#!/bin/bash

if [ ! -e .devtools ]
then
  echo "run detect-project-attributes.sh to determine your project attributes" >&2
  exit
fi

jq --version > /dev/null 2>&1
if [ $? -gt 0 ]
then
  echo "please install jq https://stedolan.github.io/jq/download" >&2
  exit 1
fi

project_name=$(jq -r '.name' .devtools)
project_type=$(jq -r '.type' .devtools)
upstream_base_image=$(jq -r '.upstream_base_image' .devtools)

if [ ! -f "Dockerfile" ]
then
  echo "getting Dockerfile" >&2
  curl -s -f https://gitlab.oit.duke.edu/rja3/developer-tools/-/raw/v1.5.2/${project_type}/container-development/Dockerfile | sed "s|{{upstream_base_image}}|${upstream_base_image}|" > Dockerfile
fi

if ! grep 'config.web_console.permissions.*my_host.*' config/environments/development.rb > /dev/null 2>&1
then
  echo "getting config/environments/development.rb"
  curl -s -f https://gitlab.oit.duke.edu/rja3/developer-tools/-/raw/v1.5.2/${project_type}/container-development/development.rb > config/environments/development.rb
fi

if ! grep 'created by devtools' docker-compose.yml > /dev/null 2>&1
then
  echo "getting docker-compose.yml" >&2
  curl -s -f https://gitlab.oit.duke.edu/rja3/developer-tools/-/raw/v1.5.2/${project_type}/container-development/docker-compose.yml > docker-compose.yml
fi

if ! grep 'created by devtools' docker-compose.override.yml > /dev/null 2>&1
then
  echo "getting docker-compose.override.yml" >&2
  depends="$(jq -r '.dbs | map("      - "+.+"\\n")[]' .devtools)$(jq -r '.services | map("      - "+.+"\\n")[]' .devtools | sed 's/\\n$//')"
  curl -s -f https://gitlab.oit.duke.edu/rja3/developer-tools/-/raw/v1.5.2/${project_type}/container-development/docker-compose.override.yml | sed "s/{{project_name}}/${project_name}/" | sed "s/{{depends}}/${depends}/g" > docker-compose.override.yml
fi

if ! grep 'created by devtools' docker-compose.local-gems.yml > /dev/null 2>&1
then
  echo "getting docker-compose.local-gems.yml" >&2
  curl -s -f https://gitlab.oit.duke.edu/rja3/developer-tools/-/raw/v1.5.2/${project_type}/container-development/docker-compose.local-gems.yml > docker-compose.local-gems.yml
fi

if ! grep 'added by devtools server.env' server.env > /dev/null 2>&1
then
  echo "getting server.env" >&2
  curl -s -f https://gitlab.oit.duke.edu/rja3/developer-tools/-/raw/v1.5.2/${project_type}/container-development/server.env > server.env
fi

if ! grep 'added by devtools server.local-prod.env' server.local-prod.env > /dev/null 2>&1
then
  echo "getting server.local-env.env" >&2
  curl -s -f https://gitlab.oit.duke.edu/rja3/developer-tools/-/raw/v1.5.2/${project_type}/container-development/server.local-prod.env > server.local-prod.env
fi

for db in $(jq -r '.dbs[]' .devtools)
do
  if ! grep "${db}\:$" docker-compose.yml > /dev/null 2>&1
  then
    echo "adding docker-compose.yml ${db} config" >&2
    curl -s -f https://gitlab.oit.duke.edu/rja3/developer-tools/-/raw/v1.5.2/${project_type}/container-development/docker-compose.${db}.yml | sed "s/{{project_name}}/${project_name}/" >> docker-compose.yml
  fi

  if [ ! -f "${db}.local.env" ]
  then
    echo "getting ${db}.env environment" >&2
    curl -s -f https://gitlab.oit.duke.edu/rja3/developer-tools/-/raw/v1.5.2/${project_type}/container-development/${db}.env > ${db}.local.env
  fi

  if [ ! -L "${db}.env" ]
  then
    ln -s ${db}.local.env ${db}.env
  fi

  if ! grep "added by devtools server.${db}.env" server.env > /dev/null 2>&1
  then
    echo "adding server.env ${db} environment" >&2
    curl -s -f https://gitlab.oit.duke.edu/rja3/developer-tools/-/raw/v1.5.2/${project_type}/container-development/server.${db}.env >> server.env
  fi

  if ! grep "added by devtools server.local-prod.${db}.env" server.local-prod.env > /dev/null 2>&1
  then
    echo "adding server.local-prod.env ${db} environment" >&2
    curl -s -f https://gitlab.oit.duke.edu/rja3/developer-tools/-/raw/v1.5.2/${project_type}/container-development/server.local-prod.${db}.env >> server.local-prod.env
  fi
done

for service in $(jq -r '.services[]' .devtools)
do
  if ! grep "${service}\:$" docker-compose.yml > /dev/null 2>&1
  then
    echo "adding docker-compose.yml ${service} config" >&2
    curl -s -f https://gitlab.oit.duke.edu/rja3/developer-tools/-/raw/v1.5.2/${project_type}/container-development/docker-compose.${service}.yml | sed "s/{{project_name}}/${project_name}/" >> docker-compose.yml
  fi

  if [ ! -f "${service}.local.env" ]
  then
    echo "getting ${service}.env environment" >&2
    curl -s -f https://gitlab.oit.duke.edu/rja3/developer-tools/-/raw/v1.5.2/${project_type}/container-development/${service}.env > ${service}.local.env
  fi

  if [ ! -L "${service}.env" ]
  then
    ln -s ${service}.local.env ${service}.env
  fi

  if ! grep "added by devtools server.${service}.env" server.env > /dev/null 2>&1
  then
    echo "adding server.env ${service} environment" >&2
    curl -s -f https://gitlab.oit.duke.edu/rja3/developer-tools/-/raw/v1.5.2/${project_type}/container-development/server.${service}.env >> server.env
  fi

  if ! grep "added by devtools server.local-prod.${service}.env" server.local-prod.env > /dev/null 2>&1
  then
    echo "adding server.local-prod.env ${service} environment" >&2
    curl -s -f https://gitlab.oit.duke.edu/rja3/developer-tools/-/raw/v1.5.2/${project_type}/container-development/server.local-prod.${service}.env >> server.local-prod.env
  fi
done

if [ ! -d "bin/docker-compose" ]
then
  mkdir -p bin/docker-compose
fi

for script in fix_permissions.sh build.sh aliases disable_local_gem.sh  enable_local_gem.sh  start_server.sh
do
  if [ ! -f "bin/docker-compose/${script}" ]
  then
    echo "getting bin/docker-compose/${script}" >&2
    curl -s -f https://gitlab.oit.duke.edu/rja3/developer-tools/-/raw/v1.5.2/${project_type}/container-development/docker-compose/${script} > bin/docker-compose/${script}
    chmod +x bin/docker-compose/${script}
  fi
done
echo "Ready for Containerization"
