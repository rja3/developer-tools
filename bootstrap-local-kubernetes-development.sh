#!/bin/bash

if [ ! -e .devtools ]
then
  echo "run detect-project-attributes.sh to determine your project attributes" >&2
  exit
fi

jq --version > /dev/null 2>&1
if [ $? -gt 0 ]
then
  echo "please install jq https://stedolan.github.io/jq/download" >&2
  exit 1
fi

ansible --version > /dev/null 2>&1
if [ $? -gt 0 ]
then
  echo "please install ansible https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html" >&2
  exit 1
fi

if ! ansible-galaxy collection list | grep oasis.ruby_kubernetes > /dev/null 2>&1
then
  echo "installing oasis ansible collection" >&2
  ansible-galaxy collection install git@gitlab.oit.duke.edu:oasis-application-development/ansible-collections/oasis.ruby_kubernetes.git
fi

project_name=$(jq -r '.name' .devtools)
project_type=$(jq -r '.type' .devtools)

if [ "$(uname -s)" == "Darwin" ]
then
  if ! command minikube > /dev/null 2>&1
  then
    echo "install Kubernetes for your machine first" >&2
    echo "curl https://gitlab.oit.duke.edu/rja3/developer-tools/-/raw/v1.5.2/install-local-kubernetes.sh | bash" >&2
    exit 1
  fi
else
  if ! command k3s > /dev/null 2>&1
  then
    echo "install Kubernetes for your machine first" >&2
    echo "curl https://gitlab.oit.duke.edu/rja3/developer-tools/-/raw/v1.5.2/install-local-kubernetes.sh | bash" >&2
    exit 1
  fi
fi

dbs="$(jq -r '.dbs[]' .devtools)"
needs_redis=$(jq -r '.services[] | select(. == "redis")' .devtools)
echo "generating helm-chart" >&2
trap 'rm -rf "${tempfile}"' EXIT
tempfile=$(mktemp --suffix .yml)

echo '- hosts: localhost' > ${tempfile}

if [ -n "${dbs}" ] || [ -n "${needs_redis}" ]
then
  echo '  vars:' >> ${tempfile}
  if [ -n "${dbs}" ]
  then
    echo '    databases:' >> ${tempfile}
    for db in ${dbs}
    do
      echo "      - ${db}" >> ${tempfile}
    done
  fi
  if [ -n "${needs_redis}" ]
  then
    echo '    needs_redis: yes' >> ${tempfile}
  fi
fi

cat <<EOF >> ${tempfile}
  collections:
    - oasis.ruby_kubernetes
  tasks:
    - name: "local_kubernetes_development"
      include_role:
        name: local_kubernetes
    - name: "helm-chart"
      include_role:
        name: helm_chart
EOF

cat ${tempfile}
ansible-playbook ${tempfile}
echo "Ready for Kubernetes Development" >&2
