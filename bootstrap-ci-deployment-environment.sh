#!/bin/bash

local_env_file="${PROJECT_NAMESPACE}.deploy.nocommit.env"
read -r -d '' usage <<'EOF'
required environment variables:
GITLAB_API_TOKEN: Personal or Project Level Access Token with api scope
PROJECT_NAMESPACE: kubernetes namespace into which applications should be deployed
HELM_TOKEN: found in email from DHTS-Automation when they created your kubernetes namespace

optional environment variables:
TARGET_PROJECT_ID: default uses the 'origin' remote in your current git working repo. Can be a numeric id, or urlencoded namespace%2Fname

creates the following Project CI Environment Variables with environment_scope "*" (e.g. across all environments), if they do not already exist:

PROJECT_NAMESPACE=required
CLUSTER_SERVER=supplied
HELM_USER=supplied
HELM_TOKEN=required

creates a file ${PROJECT_NAMESPACE}.deploy.nocommit.env file in the repo root
(you should ensure *nocommit* is in your .gitignore file)   
EOF

if [ -z "${GITLAB_API_TOKEN}" -o -z "${PROJECT_NAMESPACE}" -o -z "${HELM_TOKEN}" ]
then
    echo "${usage}" >&2
    exit
fi

origin_id=$(git remote -v | grep origin | head -1 | cut -d: -f2| cut -d. -f 1| sed 's/\//%2f/')
if [ -n "${TARGET_PROJECT_ID}" ]
then
    origin_id="${TARGET_PROJECT_ID}"
fi

if [ -f "${local_env_file}" ]
then
    "copying existing ${local_env_file} to ${local_env_file}.bak"
    mv ${local_env_file} ${local_env_file}.bak
else
    touch ${local_env_file}
fi

CLUSTER_SERVER=https://ocpconsole.duhs.duke.edu:8443
HELM_USER=helm-deployer
for dvar in PROJECT_NAMESPACE CLUSTER_SERVER HELM_USER HELM_TOKEN
do
    if curl -s -H "Authorization: Bearer ${GITLAB_API_TOKEN}" "https://gitlab.dhe.duke.edu/api/v4/projects/${origin_id}/variables/${dvar}" | grep "404 Variable Not Found" > /dev/null 2>&1
    then
        echo "setting ${dvar}" >&2
        dval="${!dvar}"
        curl -s -H "Authorization: Bearer ${GITLAB_API_TOKEN}" \
          -X POST \
          "https://gitlab.dhe.duke.edu/api/v4/projects/${origin_id}/variables" \
          --form "key=${dvar}" \
          --form "value=${dval}"  > /dev/null 2>&1
        echo "${dvar}=${dval}" >> ${local_env_file}
    else
        echo "Skipping ${dvar}, Already exists" >&2
    fi
done
