#!/bin/bash

first_arg="${1}"

case "${first_arg}" in
  --local-gems | -l)
    export COMPOSE_FILE='docker-compose.yml:docker-compose.override.yml:docker-compose.local-gems.yml'
    ;;
  --help | -h)
    echo "usage: ${0} [--help] [-h] [--local-gems] [-l]"
    echo "  --help prints help and exits"
    echo "  --local-gems sets up use of gems in /workspace_gems for local development"
    exit
    ;;
esac

if [ -n "${BASH_SOURCE[0]}" ]
then
  SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
else
  SCRIPT_DIR="${${(%):-%x}:A:h}"
fi
source ${SCRIPT_DIR}/architecture
echo "running in architecture ${architecture} with docker-compose arguments ${m1_mac_env}"
if [[ "${architecture}" != x86_* ]]
then
  if [ -n "${COMPOSE_FILE}" ]
  then
    COMPOSE_FILE="${COMPOSE_FILE}:docker-compose.m1.yml"
  else
    export COMPOSE_FILE='docker-compose.yml:docker-compose.override.yml:docker-compose.m1.yml'
  fi
fi
docker-compose run --rm ${m1_mac_env} rails db:create
docker-compose run --rm ${m1_mac_env} rails db:migrate
docker-compose run --rm ${m1_mac_env} rails db:seed
docker-compose up -d server
echo "visit http://localhost:3000 in your browser"
