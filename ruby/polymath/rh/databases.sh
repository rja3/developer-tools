#!/bin/bash

supported_database() {
  local gem=$1
cat <<EOF | grep ${gem}
  pg
  activerecord-oracle_enhanced-adapter
  sqlite3
EOF
}

install_dependencies() {
  local gem=${1}
  case "${gem}" in
    pg)
      curl -s https://gitlab.oit.duke.edu/rja3/developer-tools/-/raw/v1.5.2/ruby/polymath/$(detect-distro.sh)/postgres.sh | bash
      ;;
    activerecord-oracle_enhanced-adapter)
      curl -s https://gitlab.oit.duke.edu/rja3/developer-tools/-/raw/v1.5.2/ruby/polymath/$(detect-distro.sh)/oracle.sh | bash
      ;;
    sqlite3)
      curl -s https://gitlab.oit.duke.edu/rja3/developer-tools/-/raw/v1.5.2/ruby/polymath/$(detect-distro.sh)/sqlite.sh | bash
      ;;
  esac
}

# most projects will have 1 database, some may have more than 1
while IFS= read -r gem; do
  if supported_database "${gem}"
  then
    install_dependencies "${gem}"
  fi
done <<< $(grep "^gem\s*" Gemfile | cut -d"'" -f2 | cut -d'"' -f2)
