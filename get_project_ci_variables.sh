#!/bin/bash
: ${SOURCE_PROJECT_ID?"SOURCE_PROJECT_ID environment required."}

CI_API_V4_URL="${CI_API_V4_URL:-https://gitlab.oit.duke.edu/api/v4}"
header_file=${HEADER_FILE:-${HOME}/.dhe_gitlab_token_personal_headers}
if [ ! -f "${header_file}" ]
then
  echo "credentials file required!"
  exit 1
fi

curl -H @${header_file} ${CI_API_V4_URL}/projects/${SOURCE_PROJECT_ID}/variables | jq -r '.[] | select(.variable_type == "env_var") | .environment_scope+":"+.key+":"+.value'
