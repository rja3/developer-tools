#!/bin/bash

curl https://gitlab.oit.duke.edu/rja3/developer-tools/-/raw/v1.5.2/local_kubernetes/$(uname -s)/install.sh | bash || exit 1
echo "Ready for Local Kubernetes Development"
