VSCode Remote Container Development
---

This application is configured to allow development 
inside the application container, using 
[VSCode Remote Container Development](https://code.visualstudio.com/docs/remote/containers)


#### Getting Started

VSCode Remote Container Development requires software to be installed on the
host machine:

- [Install docker](https://docs.docker.com/get-docker/) for the operating system of your host.
- [Install docker-compose](https://docs.docker.com/compose/install/) if it is not included with the
docker installation itself (Docker for Mac and Docker for Windows both install docker-compose).
- [Install VSCode](https://code.visualstudio.com/docs/setup/setup-overview)
- [Install VSCode Remote Container Extension](https://code.visualstudio.com/docs/remote/containers#_installation)

##### Initial Setup

If you do not have a directory called `workspace` in your host computer
home directory, create an empty directory before launching VSCode.
```
mkdir ~/workspace
```

#### Usage

When you launch VSCode for this project, it will always
initially launch an editor on the host file system. Then 
it will prompt you with 
```
Folder contains a dev container configuration file. 
Reopen folder to develop in a container ([learn more](https://aka.ms/vscode-remote/docker))
```
and buttons to **Dont Show Again**, **Reopen in Container**, 
and an **X**. For most development, choose **Reopen in 
Container**. If you really just want to change a file, 
such as this file, without launching the application in 
the container, hit the **X** button, but never choose **Dont 
Show Again**, unless you always want it to open in the 
container.

When you choose **Reopen in Container** it will do the 
following:
- use the `docker-compose.yml` that we have configured for 
the project, along with VSCode extension 
docker-compose yaml files (see VSCode Configuration) with 
the Dockerfile
- build the image if the Dockerfile, or .devcontainer files have changed
or the image does not exist
- run a new container
- add VSCode libraries to that container (first time 
only)
- add ruby extensions that we have configured (first 
time only, see VSCode Configuration) to the container\
- present you with a terminal inside VSCode

VScode uses docker-compose to launch the **server**,
and all services defined in our `docker-compose.yml` file
that are defined in `.devcontainer/devcontainer.json` runServices.

When you exit VSCode, or close the window for this 
project, VSCode will run docker-compose stop, and the 
VSCode extended  **server**, and its runServices containers, will stop
(you can verify this after a few seconds using docker-compose ps
in the root of the project on the host).

When you launch VSCode again, it will not have to build 
the extended **server** container again, so it will launch your 
initial terminal faster. 

If you should happen to remove the
extended **server** container, such as with docker-compose 
down, or docker rm, then the next time you launch VSCode 
on the project, it will rebuild the extended container.

#### Working in the Container

All of the application source in the repository is 
volume mounted into the container at the path `/workspace`.

Volume mounting is a mechanism that docker supports to allow
directories on the host to be synchronized into the running container.
The path on the host does not have to match the path in the container, e.g
a directory `~/projects/foo` on the host can be mapped to any path, such as `/bar`,
in the container.

Changes in files/directories in volume mounted directories, **including** creation and
deletion, both in the container and on the host, are synchronized immediately.

The VSCode development container runs with your host machine ID.

This means:
- changes you make to the code in VSCode are saved onto the host, while also getting immediately
displayed in the application using standard hot reload functionality.
- commands run in the container, like rails g, rails d, rails db:migrate, etc., will create, delete, and
modify files and directories within the application source on the host file system.
- All files created in the container will be owned by your host machine user.

You can open many terminals in the VSCode IDE, e.g;
1. run puma
2. run rspec
3. run rails g
4. tail the development log

All of these will run inside the application container. That 
means that these run with the OS, libraries, Ruby, and gems,
built into the container, just as they would run in the production
deployed application using the `Dockerfile` we have created.

The VSCode Ruby extensions (see VSCode Configuration) also run inside the container, 
using the container OS, libraries, ruby and gems. This means that code linters, navigation
aids, integrated debuggers, etc. work as if the application is running natively on the
host.

This makes developing in a containerized application feel more like developing on 
the host machine itself, while ensuring parity between the local development
and production environment.

#### Changing Gems and Packages

Any time you change the `Dockerfile`, or add/remove gems from the `Gemfile`, you should rebuild
the VSCode container:
- hit the green button with '>< Dev Container:...' on 
the bottom right of the VSCode window, or type 
CMD-Shift-P to bring up the command template
- choose Remote-Container: Rebuild Container

This will run the same initial docker-compose command to 
rebuild the extended container, and launch inside the 
new one with a terminal.

#### Local Development

##### Running Services

Developing rails applications in the VSCode Remote Container is much like
developing them natively. The only difference is that you must use terminals
in the VSCode IDE, rather than on the host itself. Here are a couple of
patterns of development that you might use:

1. Running the application in development mode

Rails 6 supports hot reloading of both rails application code, and
assets. It supports asset reloading with the standard assets pipeline.
You will need a second terminal to run rails commands like db:create, db:migrate,
rails c, rails g, etc. You may also want a extra terminals to, run rspec, tail
logs/development.log, etc.. Here is how you would do that in 
VSCode.

  - Create a Terminal in VSCode (if it does not already exist). You can
  optionally rename the container by selecting the terminal, then typing
  CMD-Shift-P, then choose Terminal: Rename, type in **puma**, hit enter.
  - Repeat for **rails**,
  - Repeat for **log**, if desired
  - In the **rails** terminal, run `rails db:create`, `rails db:migrate`,
  `rails db:seed`
  - In the **puma** terminal, run `puma`
  
You can now navigate to http://localhost:3000 in your browser. Hot reload
of code and assets will be handled as expected.

2. Running rspec

You can work with rspec as you expect. You will need to run 
`rails db:create`, and `rails db:migrate` before you can run rspec. You must set RAILS_ENV
to test in the terminal where you run rspec or the tests will fail.
```
export RAILS_ENV=test
```

3. byebug

VSCode terminals allow you to run puma, or rspec, with byebug support.

4. web-console

The development environment configuration supports using the web-console gem
with the docker network from the local host. You can put 'console' in a
controller or view, and get a console in the browser when you visit that
controller or render that view.

##### Local Gem Development

The `.devcontainer/docker-compose.yml` automatically volume mounts a directory in
the developers host home directory, `~/workspace`, into a container directory
`/workspace_gems`. This allows you to work with gem/package directories contained
in that directory on your host filesystem inside your containerized application. For
information on modifying this file, consult the VSCode [devcontainer.json documentation](https://aka.ms/vscode-remote/devcontainer.json).

If you want to mount duke_rad gems from the local file system, you can 

- comment out the entire source block for our geminabox server to load all
duke_rad gems locally
- comment out just the duke_rad gems in the source block that you want to host
locally, but leave the ones you want to get from geminabox
- uncomment the lines of the duke_rad gems pointing to their local workspace_gems
relative path

then run the following commands in your VSCode terminal where you run puma:
(shut down puma if it is running):
```
export LOCAL_OASIS_GEMS=1
puma
```

You should **never** commit a Gemfile, or Gemfile.lock built with locally mounted
gems to git and push to gitlab. This will break the build. To ensure that you are
not doing this you can use git to restore the Gemfile and Gemfile.lock before
adding and committing code
```
git checkout -- Gemfile*
```

Changes to files in the local filesystem hosted gems will be live reloaded without
having to restart puma.

#### VSCode Configuration

The VSCode Remote Container configuration files are stored in the .devcontainer
directory. This consists of:
- `.devcontainer.json`: [this](https://code.visualstudio.com/docs/remote/containers#_devcontainerjson-reference) instructs 
VSCode how to manage remote development. We use docker-compose. 
We also ensure that the following ruby VSCode extensions
(see documentation for each online) are always added to 
the container when it is built:
  - rebornix.ruby
  - hoovercj.ruby-linter
  - vortizhe.simple-ruby-erb
  - jemmyw.rails-fast-nav
  - tmikoss.rails-latest-migration
  - kaiwood.endwis
- `docker-compose.yml`: this extends the project 
docker-compose.yml with VSCode specific features. 
- `docker-compose.user.yml`: this extends the project docker-compose.yml to run
the development container with your host user ID.

**Notes**
- this will build your server using your Dockerfile if it does not exist, which can take a few minutes. You can click
`show log` in the pop-up in the lower part of the VSCode screen during the build to see its progress. Note, this can
prevent VSCode from automatically starting a terminal in the container. If so, you can manually launch one when the build
finishes.
- it does not actually start up the application as a server

This creates a development environment running in a container using 
the same image that will ultimately be used to deploy the application. You can open as many VSCode terminals in this container as you need, and
they all inherit the environment specified in the project
docker-compose.yml file for the server.

You should work in this development environment exactly as you would
running on your host, e.g. you will need to run bundle install before
you can run anything else.

