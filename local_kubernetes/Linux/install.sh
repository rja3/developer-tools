#!/bin/bash

check_required_software() {
  if ! command kubectl > /dev/null 2>&1
  then
    echo "please install kubectl and helm" >&2
    echo "https://kubernetes.io/docs/tasks/tools/" >&2
    echo "https://helm.sh/docs/intro/install/" >&2
    exit 1
  fi

  if ! command helm > /dev/null 2>&1
  then
    echo "please install helm" >&2
    echo "https://helm.sh/docs/intro/install/" >&2
    exit 1
  fi
}

install_k3s() {
  if ! command k3s > /dev/null 2>&1
  then
    echo "installing k3s" >&2
    curl -o /tmp/get_k3s.sh -sfL https://get.k3s.io
    chmod +x /tmp/get_k3s.sh
    /tmp/get_k3s.sh --docker
  fi
}

start_k3s() {
  if [ "$(systemctl is-active k3s)" != "active" ]
  then
    echo "starting k3s" >&2
    sudo systemctl start k3s
    echo "waiting for k3s to start" >&2
    sleep 10
    while ! sudo k3s kubectl get nodes 2> /dev/null | grep '\bReady'
    do
      echo "waiting for node to be ready" >&2
      sleep 5
    done
    configure_user_kubectl
  fi
}

configure_user_kubectl() {
  while [ ! -f "/etc/rancher/k3s/k3s.yaml" ]
  do
    echo "waiting for kubectl config" >&2
    sleep 5
  done

  if [ -f "${HOME}/.kube/config" ]
  then
    echo "${HOME}/.kube/config exists" >&2
    echo "moving to ${HOME}/.kube/config.bak" >&2
    mv ~/.kube/config ~/.kube/config.bak
  fi
  
  sudo cp /etc/rancher/k3s/k3s.yaml ~/.kube/config
  sudo chown $(id -u) ~/.kube/config
  chmod 700 ~/.kube/config
}

if [ "$EUID" -ne 0 ]
then
  echo "some commands will run with sudo" >&2
  sleep 5
fi
echo "installing for ${USER}" >&2
check_required_software
if ! echo "${PATH}" | grep '/usr/local/bin'
then
  export PATH="/usr/local/bin:${PATH}"
fi
install_k3s
start_k3s
echo 'k3s is running'
