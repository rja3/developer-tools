# developer-tools

This is a collection of tools to help developers work with containers.

This documentation is for version v1.5.2

[[_TOC_]]

## Preparing Your Developer Machine

The developer tools depend on the existence of the following software installed
on your developer machine:

- curl: almost any modern *nix (linux, mac os) will have curl installed. There is also
a [windows installation](https://curl.se/windows/)
- jq: an extremely useful cli to process and manipulate JSON. [Installation Instructions](https://stedolan.github.io/jq/download)
- docker: required to build and run containerized applications. You can find
documentation to install either docker-ce or moby for various linux distributions
online. For the Mac or Windows, install [Docker Desktop](https://www.docker.com/get-started)
- docker-compose: This allows you to write configuration as code that all developers
use to develop your application on their developer machine using docker. This is
installed with Docker Desktop, and may also be installed with the docker distributions
for linux.

## Base Images for Local Development

You can use any available upstream image to build your project local development image. The developer tools system
can detect which databases your project needs and include the library requirements in the local development image
when you run build_project_local_development_image.sh (see below).

## Atomic Scripts

All of the below scripts are atomic, meaning that they will not overwrite any files on your filesystem if those files already
exist. This makes it possible to regenerate only a specific file (if the devtools template changes, or you want to use a
different version of ruby in your Dockerfile) by removing just the file(s) that you want to regenerate before running the
script.

## Detect Project Attributes

This will detect attributes about your project using common files created in the root of the project
directory by the supported application development environments.

The following types of projects can be detected

### ruby

Ruby projects must contain a Gemfile in the root of the project directory.

#### Ruby Project Attributes

name: name of the parent directory containing the project

type: 'ruby'

version:  Most recent rails will generate a .ruby_version file that looks like
```
ruby-2.7.2
```

If this file does not exist, you can create it, or add a line in your Gemfile like:
```
ruby '~> 2.7.0'
```

This instructs your Gemfile to fail to bundle if the ruby you are using
is not 2.7, but it will not fail if the patch version changes. This is
good practice in ruby projects, as it guarantees that you are working
with a compatible ruby. **Note** If you already have a like like

```
ruby '2.7.5'
```

You can add a ~> to the version string like so:
```
ruby '~> 2.7.5'
```
and it will detect the version of ruby that you are using correctly.


upstream_base_image: This defaults to the official `ruby:${version}` for your ruby version.
Official ruby base images are well maintained, and frequently patched to remove security 
vulnerabilities. 
You can override the base image that you want to use for your project by setting the 
PROJECT_UPSTREAM_BASE_IMAGE environment variable to the full name you would use to docker 
pull the image.

services: The script uses your Gemfile to determine which external services your application requires. Other scripts in the
developer_tools, such as bootstrap-container-development.sh (see below) use these services to inform which configuration, and
external services must be present for the application to run. The only supported service at this time is redis, added when
your Gemfile includes the `redis` gem. Others may be added in the future.

dbs: The script uses your Gemfile to determine which database, or databases, your project requires. Other scripts in the
developer_tools, such as bootstrap-container-development.sh (see below) use these dbs to inform which db configuration and
services must be present for the application to run. Only the following dbs are supported:

- postgres: detected when your Gemfile includes the `pg` gem
- oracle: detected when your Gemfile includes the `activerecord-oracle_enhanced-adapter` gem
- sqlite: detected when your Gemfile includes the `sqite3` gem.

A rails project can use multiple dbs, but most only use one.

#### Generate your project attributes
To get started, run the following from a terminal:

```bash
curl https://gitlab.oit.duke.edu/rja3/developer-tools/-/raw/v1.5.2/detect-project-attributes.sh | bash
```

This will create a .devtools file in your root directory if it does
not already exist. If it does exist, it will not change it. To regengerate
it, remove it and rerun.

## Bootstrap Containerization and Local docker-compose Development

This will add a Dockerfile to your project, and basic docker-compose.yml with configuration to start building and
developing your application. This docker-compose can be used alone, or with VSCode Remote Container Development. You must
run detect-project-attributes.sh before running this.

The Dockerfile uses the polymath installation system (see below). This script uses your .devtools to craft your Dockerfile
with the specified upstream_base_image, and generate configuration for docker-compose for the dbs and services that your
application uses.

To get started:
- run detect-project-attributes.sh if you have not already
- run the following from a terminal:

```bash
curl https://gitlab.oit.duke.edu/rja3/developer-tools/-/raw/v1.5.2/bootstrap-container-development.sh | bash
```

This will create new files in your project that should be committed.
- Dockerfile: Dockerfile for use locally and in production
- docker-compose.yml: basic docker-compose for running your application stack
- .docker-compose.override.yml: extends docker-compose.yml for local
development with docker-compose.
- various *.env files: these are environment files to configure the services in the docker-compose.yml
- docker-compose.local-gems.yml: extends docker-compose to mount the host ~/workspace directory into /workspace_gems directory in the container, and sets the LOCAL_OASIS_GEMS environment variable to 1 to support live reloading of local gems hosted outside Rails.root in the container file system. This is meant to be used with the bin/docker-compose scripts.
- bin/docker-compose: set of scripts to make it easy to build and rebuild your project container image, start the server with or without support for local gem development, and configure bundle to
use local path hosted gems in /workspace_gems.

This script also overwrites config/environments/development.rb with settings that
facilitate development in a container, including:
- autodetection of container IP to use to allow use of the rails browser console
- the presence an environment variable LOCAL_OASIS_GEMS determines the way rails live reloads in response to changes in your local files. The default rails method is to use `ActiveSupport::EventedFileUpdateChecker`. This only responds to changes to files in/under Rails.root. If you mount gems outside of Rails.root in the container, you should set this environment variable before running puma, and it will change to use `ActiveSupport::FileUpdateChecker`, which will respond to changes in those locally hosted gem files located outside of Rails.root.

**Note** if you have other settings in your config/environment/development.rb
file, you should merge those settings in with the changes that this file applies.

### Polymath Dockerfile Installation System

`ruby/polymath/polymath.sh` is used to build every project container image for each supported project type, and to install the
requirements that your application needs. It is designed to infer the linux flavor of the upstream_base_image during the build
(supported flavors are `deb`, and `rh`), and use scripts specific for that flavor of linux to install the required libraries
that are needed, based on inferences from common project specific files:

ruby version: detects whether the specific version of ruby specified in .ruby_version or Gemfile is present in the upstream_base_image.
If it is not present, this will install that version of ruby into the image.

dbs: for each supported db required by gems in your Gemfile, this will install the required libraries into the image if they
are not already present, and configure the image to use them correctly. For oracle this installs Oracle Instant Client version
19.12 (this may be configurable in the future). If your applications requires more than 1 db, libraries for all required dbs
will be installed.

nodejs: if your project has a package.json file, this will install nodejs version 16. You can create a file called
.nodejs-major-version, with the numeric major version (must be at least a MAINTENANCE LTS version [here](https://nodejs.org/en/about/releases/)

yarn: if your project has a yarn.lock file, this will install yarn, and run `yarn install --check-files --frozen-lockfile --non-interactive`
in the final, production image.

## Bootstrapping VSCode Remote Container Development

If you use VSCode to develop your application, you can use 
[VSCode Remote Container Development](https://code.visualstudio.com/docs/remote/containers) to develop your code inside a container made from the
same image that will ultimately be deployed into production. You must run
detect-project-type.sh and bootstrap-container-development.sh before running this.

To get started:
- install the [VSCode Remote Container Development](https://code.visualstudio.com/docs/remote/containers-tutorial) 
extension
- run detect-project-attributes.sh if you have not already
- bootstrap local docker-compose development
- run the following from a terminal:

```bash
curl https://gitlab.oit.duke.edu/rja3/developer-tools/-/raw/v1.5.2/bootstrap-vscode.sh | bash
```

This will create new files in your project that should be committed.
- VSCode.md: documentation on using VSCode Remote Container Development
- .devcontainer: contains configuration files for VSCode Remote Container
Development. You can change these to fit your projects needs, but be 
careful. You should not change the docker-compose.yml or docker-compose.user.nocommit.yml images.
Run this script with a new local_development_image in your .devtools file to regenerate them.

## Bootstrapping Project Kubernetes Deployment Environment

The Oasis [deployment utilities](https://gitlab.oit.duke.edu/ori-rad/ci-pipeline-utilities/deployment#helm-deployment-environment) require a set of [Gitlab CI Variables](https://docs.gitlab.com/ee/ci/variables)
for it to authentication to the DHE Kubernetes Cluster and deploy applications.
This script will provision the following variables into a gitlab project, available for
all deployment environments

It requires the following environment variables be set:
GITLAB_API_TOKEN: [Personal](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) or [Project Level](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html) Access Token with `api` scope
PROJECT_NAMESPACE: kubernetes namespace into which applications should be deployed
HELM_TOKEN: found in email from DHTS-Automation when they created your kubernetes namespace

It can take the following optional environment variables:
TARGET_PROJECT_ID: default uses the 'origin' remote in your current git working repo. 
Can be a numeric id, or urlencoded namespace%2Fname where namespace must also be
urlenceded ('/' == %2f)

When run, this script creates the following Project CI Environment Variables with environment_scope "*" (e.g. across all environments), if they do not already exist:

PROJECT_NAMESPACE=required env
CLUSTER_SERVER=supplied
HELM_USER=supplied
HELM_TOKEN=required env

It also creates a file ${PROJECT_NAMESPACE}.deploy.nocommit.env file in the repo
root (you should ensure *nocommit* is in your .gitignore file).

To use it:
- set the environment variables
```
export PROJECT_NAMESPACE='yournamespace'
export GITLAB_API_TOKEN='your gitlab token'
export HELM_TOKEN='your helm token'
```
- run the following:
```bash
curl https://gitlab.oit.duke.edu/rja3/developer-tools/-/raw/v1.5.2/bootstrap-ci-deployment-environment.sh | bash
```

## Managing Project CI Variables

Many projects have CI variables that are required for CI testing and
deployment. The following scripts can be copied down to your local file system, and used to manage variables in your projects. Unfortunately, these
cannot be used with curl piped to bash.

### create_project_ci_variables

Requires a TARGET_PROJECT_ID which can be the integer id of the target
project, or the namespaced name of the project, urlencoded. It also requires a header credentials file (see below). Most project
names can be turned into ids by substituting '/' with '%2f', e.g. for a project named foo/bar, the TARGET_PROJECT_ID would be foo%2fbar.

Takes stdin consisting of 1 or more lines in the format
{environment_scope}:{key}:{value}

and creates the TARGET_PROJECT_ID variables with the given environment 
scope. If a variable key already exists for the environment_scope, that 
variable will fail to be created. Use update_project_ci_variables to 
update existing variables for the given environment_scope.

see [environment scope](https://docs.gitlab.com/ee/ci/environments/index.html#scoping-environments-with-specs) for what is allowed.

### update_project_ci_variables.sh

Takes the same environment, credentials_file, and stdin input as 
create_project_ci_variables, and updates each variable by key and 
environment_scope. Will fail if the key does not exist for the provided 
environment_scope.

### get_project_ci_variables.sh

This requires a SOURCE_PROJECT_ID environment variable, which takes the
same form as TARGET_PROJECT_ID in the previous scripts. It also requires the header credentials file (see below). It prints out
all environment variables of variable_type == 'env_var' (it does not
print file type variables) in the format:
{environment_scope}:{key}:{value}

### credentials file

Each of the 3 scripts above requires a credentials file on your file 
system in the form of an http Bearer Token Authorization Header. The
default credentials file should be in your home directory, and named
`.dhe_gitlab_token_personal_headers`. This can be overridden by setting
the HEADER_FILE environment variable to the absolute path to a file on
your file system. This file should take the form:

Authorization: Bearer {TOKEN}

where TOKEN is one of two things:
- a [personal access token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html)
- a [project access token](https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html) with read_repository and write_repository scopes to the project specified in TARGET_PROJECT_ID,
or SOURCE_PROJECT_ID.

## Example Documentation

- [0 to Containerized Rails in 5 minutes](https://gitlab.oit.duke.edu/rja3/developer-tools/-/blob/main/ZeroToContainerizedRails.md)

- [0 to Local Kubernetes Rails](https://gitlab.oit.duke.edu/rja3/developer-tools/-/blob/main/ZeroToLocalKubernetesRails.md)
