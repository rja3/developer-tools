#!/bin/bash

apt-get upgrade -y
apt-get install -y --no-install-recommends zlib1g-dev build-essential postgresql-client libpq5 libpq-dev
