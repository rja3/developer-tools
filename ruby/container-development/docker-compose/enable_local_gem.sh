#!/bin/bash
  
if [ "$#" == "0" ]
then
  echo "usage: ${0} gem_name [gem2_name]"
  echo "  each gem_name directory must exist in the ${HOME}/workspace on your host"
  exit 1
fi

export COMPOSE_FILE='docker-compose.yml:docker-compose.override.yml:docker-compose.local-gems.yml'
if [ -n "${BASH_SOURCE[0]}" ]
then
  SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )
else
  SCRIPT_DIR="${${(%):-%x}:A:h}"
fi
source ${SCRIPT_DIR}/architecture
if [[ "${architecture}" != x86_* ]]
then
  COMPOSE_FILE="${COMPOSE_FILE}:docker-compose.m1.yml"
fi

while (( "$#" ))
do
  gem_name="${1}"
  if [ -d "${HOME}/workspace/${gem_name}" ]
  then
    docker-compose exec server bundle config set local.${gem_name} /workspace_gems/${gem_name}
    docker-compose exec server bundle
    docker-compose exec server bundle info ${gem_name}
    shift
  else
    echo "${HOME}/workspace/${gem_name} does not exist"
    exit 1
  fi
done
docker-compose restart server
