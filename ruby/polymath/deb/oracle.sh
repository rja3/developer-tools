#!/bin/bash

set -e
# ORACLE_VERSION and ORACLE_MAJOR_VERSION must be supported on linux by
# Oracle
# https://www.oracle.com/database/technologies/instant-client/linux-x86-64-downloads.html
# https://www.oracle.com/database/technologies/instant-client/linux-arm-aarch64-downloads.html
if [ "$(uname -m)" == "x86_64" ]
then
  architecture='x64'
  ORACLE_VERSION='19.10.0.0.0'
  ORACLE_VERSION_SLUG='191000'
  ORACLE_MAJOR_VERSION='19_10'
else
  architecture='arm64'
  ORACLE_VERSION='19.10.0.0.0'
  ORACLE_VERSION_SLUG='191000'
  ORACLE_MAJOR_VERSION='19_10'
fi

if /opt/oracle/instantclient_${ORACLE_MAJOR_VERSION}/sqlplus -v | grep "${ORACLE_VERSION}" > /dev/null 2>&1
then
  echo "ORACLE ${ORACLE_VERSION} detected, nothing to do"
  exit
fi
export ORACLE_HOME="/opt/oracle/instantclient_${ORACLE_MAJOR_VERSION}"
export NLS_LANG=AMERICAN_AMERICA.UTF8
export LD_LIBRARY_PATH=/opt/oracle/instantclient_${ORACLE_MAJOR_VERSION}

mkdir /opt/oracle

cd /opt/oracle
apt-get update -qq
apt-get upgrade -y
apt-get install -y --no-install-recommends unzip libaio-dev
wget -q https://download.oracle.com/otn_software/linux/instantclient/${ORACLE_VERSION_SLUG}/instantclient-basic-linux.${architecture}-${ORACLE_VERSION}dbru.zip
wget -q https://download.oracle.com/otn_software/linux/instantclient/${ORACLE_VERSION_SLUG}/instantclient-sqlplus-linux.${architecture}-${ORACLE_VERSION}dbru.zip
wget -q https://download.oracle.com/otn_software/linux/instantclient/${ORACLE_VERSION_SLUG}/instantclient-sdk-linux.${architecture}-${ORACLE_VERSION}dbru.zip
unzip instantclient-basic-linux.${architecture}-${ORACLE_VERSION}dbru.zip
unzip instantclient-sqlplus-linux.${architecture}-${ORACLE_VERSION}dbru.zip
unzip instantclient-sdk-linux.${architecture}-${ORACLE_VERSION}dbru.zip
echo ${LD_LIBRARY_PATH} > /etc/ld.so.conf.d/oracle-instantclient.conf
ldconfig
if [ "${architecture}" == "arm64" ]
then
  apt -y install patchelf
  patchelf --set-interpreter /lib/ld-linux-aarch64.so.1 instantclient_19_10/sqlplus
  patchelf --set-interpreter /lib/ld-linux-aarch64.so.1 instantclient_19_10/adrci
  patchelf --set-interpreter /lib/ld-linux-aarch64.so.1 instantclient_19_10/genezi
  patchelf --set-interpreter /lib/ld-linux-aarch64.so.1 instantclient_19_10/uidrvci
  apt-get purge -y patchelf
fi
apt-get purge -y unzip
rm instantclient-basic-linux.${architecture}-${ORACLE_VERSION}dbru.zip
rm instantclient-sqlplus-linux.${architecture}-${ORACLE_VERSION}dbru.zip
rm instantclient-sdk-linux.${architecture}-${ORACLE_VERSION}dbru.zip
echo "#added by oasis developer tools oracle.sh" >> /etc/profile.d/oracle.sh
echo "export PATH=\"${PATH}:/opt/oracle/instantclient_${ORACLE_MAJOR_VERSION}\"" >> /etc/profile.d/oracle.sh
echo "export ORACLE_HOME=/opt/oracle/instantclient_${ORACLE_MAJOR_VERSION}" >> /etc/profile.d/oracle.sh
echo "export NLS_LANG=AMERICAN_AMERICA.UTF8" >> /etc/profile.d/oracle.sh
echo "export LD_LIBRARY_PATH=/opt/oracle/instantclient_${ORACLE_MAJOR_VERSION}" >> /etc/profile.d/oracle.sh
