#!/bin/bash

jq --version > /dev/null 2>&1
if [ $? -gt 0 ]
then
  echo "please install jq https://stedolan.github.io/jq/download" >&2
  exit 1
fi

if [ ! -e .devtools ]
then
  echo "run detect-project-attributes.sh to determine your project attributes" >&2
  exit
fi

project_name=$(jq -r '.name' .devtools)
project_type=$(jq -r '.type' .devtools)

if [ ! -d '.devcontainer' ]
then
  mkdir -p '.devcontainer'
fi

if [ ! -f '.devcontainer/devcontainer.json' ]
then
  echo "getting .devcontainer/devcontainer.json" >&2
  runServices=$(jq -rc '[.dbs, .services] | transpose[0]' .devtools)
  curl -s -f https://gitlab.oit.duke.edu/rja3/developer-tools/-/raw/v1.5.2/${project_type}/devcontainer/devcontainer.json | sed "s/{{project_name}}/${project_name} container/" | jq --argjson runServices "${runServices}" '.runServices=$runServices' > .devcontainer/devcontainer.json
  if [ $? -gt 0 ]
  then
    echo "could not bootstrap vscode for type ${project_type}" >&2
    exit
  fi
fi

if [ ! -f '.devcontainer/docker-compose.yml' ]
then
  echo "getting .devcontainer/docker-compose.yml" >&2
  curl -s -f https://gitlab.oit.duke.edu/rja3/developer-tools/-/raw/v1.5.2/${project_type}/devcontainer/docker-compose.yml | sed "s|{{project_name}}|${project_name}|" > .devcontainer/docker-compose.yml
  if [ $? -gt 0 ]
  then
    echo "could not bootstrap vscode for type ${project_type}" >&2
    exit
  fi
fi

if [ ! -f ".devcontainer/getting docker-compose.user.nocommit.yml" ]
then
  echo ".devcontainer/getting docker-compose.user.nocommit.yml" >&2
  curl -s -f https://gitlab.oit.duke.edu/rja3/developer-tools/-/raw/v1.5.2/${project_type}/devcontainer/docker-compose.user.yml | sed "s|{{uid}}|${UID}|" > .devcontainer/docker-compose.user.nocommit.yml
  if [ $? -gt 0 ]
  then
    echo "could not bootstrap vscode for type ${project_type}" >&2
    exit
  fi
fi

if [ ! -f 'VSCode.md' ]
then
  echo "getting VSCode.md" >&2
  curl -s -f https://gitlab.oit.duke.edu/rja3/developer-tools/-/raw/v1.5.2/${project_type}/devcontainer/VSCode.md > VSCode.md
  if [ $? -gt 0 ]
  then
    echo "could not bootstrap vscode for type ${project_type}" >&2
    exit
  fi
fi

grep '*nocommit*' .gitignore > /dev/null 2>&1 || echo '*nocommit*' >> .gitignore

echo "Ready For VSCode Remote Container Development" >&2
echo "consult VSCode.md for information" >&2
echo "you should commit these changes to git" >&2