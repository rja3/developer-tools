#!/bin/bash

if (! command -v curl > /dev/null 2>&1)
then
    if (command -v apt-get > /dev/null 2>&1)
    then
        apt-get update -qq
        apt-get upgrade -y
        apt-get install -y --no-install-recommends curl
    else 
        dnf install -y curl
    fi
fi

curl -s https://gitlab.oit.duke.edu/rja3/developer-tools/-/raw/v1.5.2/ruby/polymath/detect-distro.sh > /usr/local/bin/detect-distro.sh
chmod +x /usr/local/bin/detect-distro.sh
curl -s https://gitlab.oit.duke.edu/rja3/developer-tools/-/raw/v1.5.2/ruby/polymath/$(detect-distro.sh)/base.sh | bash
curl -s https://gitlab.oit.duke.edu/rja3/developer-tools/-/raw/v1.5.2/ruby/polymath/$(detect-distro.sh)/asset_pipeline.sh | bash
curl -s https://gitlab.oit.duke.edu/rja3/developer-tools/-/raw/v1.5.2/ruby/polymath/$(detect-distro.sh)/ruby.sh | bash
curl -s https://gitlab.oit.duke.edu/rja3/developer-tools/-/raw/v1.5.2/ruby/polymath/$(detect-distro.sh)/databases.sh | bash
curl -s https://gitlab.oit.duke.edu/rja3/developer-tools/-/raw/v1.5.2/ruby/polymath/$(detect-distro.sh)/cleanup.sh | bash
chgrp -R root $(gem env gemdir)
chmod -R g=rwX $(gem env gemdir)
if [ -d "$(gem env | grep 'EXECUTABLE DIRECTORY' | cut -d' ' -f 6)" ]
then
  chgrp -R root $(gem env | grep 'EXECUTABLE DIRECTORY' | cut -d' ' -f 6)
  chmod -R g=rwX $(gem env | grep 'EXECUTABLE DIRECTORY' | cut -d' ' -f 6)
fi
