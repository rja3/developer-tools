#!/bin/bash

set -e
# ORACLE_VERSION and ORACLE_MAJOR_VERSION must be supported on linux by
# Oracle
# https://www.oracle.com/database/technologies/instant-client/linux-x86-64-downloads.html
# https://www.oracle.com/database/technologies/instant-client/linux-arm-aarch64-downloads.html
architecture="$(uname -m)"
if [ "${architecture}" == "x86_64" ]
then
  ORACLE_VERSION='21.7.0.0.0'
  ORACLE_VERSION_SLUG='217000'
  ORACLE_MAJOR_VERSION='21'
  download_base_uri="https://download.oracle.com/otn_software/linux/instantclient/${ORACLE_VERSION_SLUG}/oracle-instantclient"
else
  ORACLE_VERSION='19.10.0.0.0'
  ORACLE_VERSION_SLUG='191000'
  ORACLE_MAJOR_VERSION='19.10'
  download_base_uri="https://download.oracle.com/otn_software/linux/instantclient/${ORACLE_VERSION_SLUG}/oracle-instantclient${ORACLE_MAJOR_VERSION}"
fi

if /usr/lib/oracle/${ORACLE_MAJOR_VERSION}/client64/bin/sqlplus -v | grep "${ORACLE_VERSION}" > /dev/null 2>&1
then
  echo "ORACLE ${ORACLE_VERSION} detected, nothing to do"
  exit
fi

export ORACLE_HOME="/usr/lib/oracle/${ORACLE_MAJOR_VERSION}/client64"
export NLS_LANG=AMERICAN_AMERICA.UTF8
export LD_LIBRARY_PATH=/usr/lib/oracle/${ORACLE_MAJOR_VERSION}/client64/lib

dnf install -y libnsl
dnf install -y ${download_base_uri}-basic-${ORACLE_VERSION}-1.${architecture}.rpm
dnf install -y ${download_base_uri}-sqlplus-${ORACLE_VERSION}-1.${architecture}.rpm
dnf install -y ${download_base_uri}-devel-${ORACLE_VERSION}-1.${architecture}.rpm
echo ${LD_LIBRARY_PATH} > /etc/ld.so.conf.d/oracle-instantclient.conf
ldconfig
if [ "${architecture}" == "aarch64" ]
then
  dnf install -y patchelf
  patchelf --set-interpreter /lib/ld-linux-aarch64.so.1 /usr/lib/oracle/${ORACLE_MAJOR_VERSION}/client64/bin/sqlplus
  patchelf --set-interpreter /lib/ld-linux-aarch64.so.1 /usr/lib/oracle/${ORACLE_MAJOR_VERSION}/client64/bin/adrci
  patchelf --set-interpreter /lib/ld-linux-aarch64.so.1 /usr/lib/oracle/${ORACLE_MAJOR_VERSION}/client64/bin/genezi
fi
echo "# added by oasis developer tools oracle.sh" >> /etc/profile.d/oracle.sh
echo "export PATH=\"${PATH}:/usr/lib/oracle/${ORACLE_MAJOR_VERSION}/client64/bin\"" >> /etc/profile.d/oracle.sh
echo "export ORACLE_HOME=/usr/lib/oracle/${ORACLE_MAJOR_VERSION}/client64" >> /etc/profile.d/oracle.sh
echo "export NLS_LANG=AMERICAN_AMERICA.UTF8" >> /etc/profile.d/oracle.sh
echo "export LD_LIBRARY_PATH=/usr/lib/oracle/${ORACLE_MAJOR_VERSION}/client64" >> /etc/profile.d/oracle.sh
