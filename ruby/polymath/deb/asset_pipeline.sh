#!/bin/bash

if [ -f "package.json" ]
then
  nodejs_major_version=16
  if [ -f ".nodejs-major-version" ]
  then
    nodejs_major_version=$(cat .nodejs-major-version)
  fi

  if ! (node --version | grep "v${nodejs_major_version}") > /dev/null 2>&1
  then
    echo "installing nodejs ${nodejs_major_version}" >&2
    curl -sL https://deb.nodesource.com/setup_${nodejs_major_version}.x | bash -
    apt-get install -y --no-install-recommends nodejs
    npm install -g npm@next
  fi
fi

if [ -f "yarn.lock" ]
then
  if ! yarn --version > /dev/null 2>&1
  then
    echo "installing yarn" >&2
    curl -sL https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
    echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
    apt-get install -y --no-install-recommends yarn
  fi
fi
